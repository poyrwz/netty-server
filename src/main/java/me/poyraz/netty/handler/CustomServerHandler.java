package me.poyraz.netty.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class CustomServerHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

	public static List<ChannelHandlerContext> ctxs = Collections.synchronizedList(new ArrayList<ChannelHandlerContext>());
	public static Map<String, String> mapMute = new HashMap<String, String>();

	@Override
	protected void channelRead0(ChannelHandlerContext chc, TextWebSocketFrame txtMsg) throws Exception {
		String messReceived = txtMsg.text();
		System.out.println(messReceived);
		chc.channel().writeAndFlush("Selam client!");
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		Channel incoming = ctx.channel();
		incoming.writeAndFlush("hello");
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {

	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		ctxs.remove(ctx);
		super.channelInactive(ctx);
		System.out.println("remove " + ctx);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		ctxs.add(ctx);
		super.channelActive(ctx);
		System.out.println("add " + ctx);
	}

}
